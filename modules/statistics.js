var mysql = require('mysql');
var successCall = null;
module.exports = exports = {

    statistics: function (limitFirst, limitSecond, offsetFirst, offsetSecond, numberCalculate,
                          start_year, number_stack, stage, minNumber, response) {
        this.res = response;
        this.limitFirst = limitFirst;
        this.limitSecond = limitSecond;
        this.offsetFirst = offsetFirst;
        this.offsetSecond = offsetSecond;
        this.numberCalculate = numberCalculate;
        this.startYear = start_year;
        this.numberStack = number_stack;
        this.numberStage = stage;
        this.minimalCount = minNumber;
        this.limitOffset = this.numberStack + '_' + this.numberStage + '_' + this.limitFirst + '_' + this.offsetFirst + '-' +
                           this.numberStack + '_' + parseInt(this.numberStage + 1) + '_' + this.limitSecond + '_' +
                           this.offsetSecond;

        this.ending = false;
        this.successCallback = null;

        var database = function (fieldInsert) {
            this.tableName = 'kcms_statistics';
            this.connection = mysql;
            this.field = fieldInsert;

            this.connectionMysql = this.connection.createPool({
                host    : 'localhost',
                password: '1234',
                user    : 'kenocms',
                database: 'kenocms',
                supportBigNumbers   : true,
                connectTimeout      : 700000,
                connectionLimit     : 5,
                queueLimit          : 5
            });

            this.currentStep = 0;

            this.countSave = 0;

            this.selectData = function (limit, offset, stage, stack, numberCalculate, start_year, success) {
                var queryString  = 'SELECT * FROM ' + this.tableName + ' WHERE ';
                queryString += 'number_stack = ' + stack + ' AND ';
                queryString += 'stage = ' + stage + ' AND ';
                queryString += 'number_calculate = ' + numberCalculate+ ' AND ';
                queryString += 'start_year = ' + start_year;
                queryString += ' LIMIT ' + limit + ', ' + offset;

                this.connectionMysql.getConnection(function (err, connection) {
                    connection.query(queryString, function (err, row) {
                        connection.release();
                        success(err, row)
                    });
                });
            };

            this.dataAdd = function (insertField, limit) {
                this.limit = 10000000;
                this.field = insertField;
                this.dataColumn = [];
                this.addData = function (data, end) {
                    if (typeof end === "undefined") {
                        end = false;
                    }

                    dataAddSelf.dataColumn[dataAddSelf.countSave] = data;
                    dataAddSelf.countSave++;

                    if (end || dataAddSelf.limit <= dataAddSelf.dataColumn.length) {
                        this.insert();
                    }
                };

                this.ending = false;

                this.insert = function (end) {
                    dataAddSelf.ending = end;
                    if (!dataAddSelf.dataColumn.length) {
                        if (end) {
                            var s = self.successCallback ? self.successCallback : successCall;
                            s(self.res, {
                                count: dataAddSelf.countSave,
                                url: ''
                            });
                        }
                        return;
                    }

                    var queryString = 'INSERT INTO kcms_statistics (' + dataSelf.field + ') VALUES ' +
                                      '(' + dataAddSelf.dataColumn.join('), (') + ')';

                    if (typeof self.connectionMysql === "undefined") {
                        self.connectionMysql = this.connection.createPool({
                            host    : 'localhost',
                            password: '1234',
                            user    : 'kenocms',
                            database: 'kenocms',
                            supportBigNumbers   : true,
                            connectTimeout      : 700000,
                            connectionLimit     : 5,
                            queueLimit          : 5
                        });

                        //self.connectionMysql.connect();
                    }

                    self.connectionMysql.getConnection(function (err, connection) {
                        connection.query(queryString, function (err, row) {
                            connection.release();
                            var s = self.successCallback ? self.successCallback : successCall;
                            s(self.res, {
                                count: dataAddSelf.countSave,
                                url: ''
                            });
                        });
                    });

                    //delete dataSelf.dataColumn;
                    dataSelf.dataColumn = [];
                };

                var dataAddSelf = this;

                return this;
            };

            this.addDataTable = this.dataAdd(field);

            var dataSelf = this;

            return this;
        };

        this.connector = database();

        this.calculate = function (success) {
            self.checkExists(this.getData);
            self.successCallback = success;
            successCall = success;
        };

        this.getDataQuery = function (second) {
            if (typeof second === "undefined")
                second = false;

            var stage = second ? parseInt(parseInt(self.numberStage) + 1) : self.numberStage;
            var limit = second ? self.limitSecond : self.limitFirst;
            var offset = second ? self.offsetSecond : self.offsetFirst;

            var querystring = 'SELECT * FROM ' + self.connector.tableName + ' WHERE '
                              + 'number_stack=' + parseInt(self.numberStack) + ' AND '
                              + 'stage=' + stage + ' AND '
                              + 'number_calculate=' + self.numberCalculate + ' AND '
                              + 'start_year=' + self.startYear
                              + ' LIMIT ' + offset + ', ' + limit;

            return querystring;
        };

        this.dataFirst = [];
        this.dataSecond = [];
        this.countCount = 0;

        this.getData = function (second) {
            if (typeof second === "undefined")
                second = false;

            if (typeof self.connectionMysql === "undefined") {
                self.connectionMysql = this.connection.createPool({
                    host    : 'localhost',
                    password: '1234',
                    user    : 'kenocms',
                    database: 'kenocms',
                    supportBigNumbers   : true,
                    connectTimeout      : 700000,
                    connectionLimit     : 5,
                    queueLimit          : 5
                });

                //self.connectionMysql.connect();
            }


            self.connectionMysql.getConnection(function (err, connection) {
                connection.query(self.getDataQuery(second), function (err, row) {
                    connection.release();
                    if (err) {
                    } else {
                        if (second)
                            self.dataSecond = row;
                        else {
                            self.dataFirst = row;
                        }

                        if (second && self.dataFirst.length && self.dataSecond.length) {
                            // calculate
                            self.saveStat();
                        } else {
                            if (second) {
                                console.log(2);
                                //end
                                var s = self.successCallback ? self.successCallback : successCall;
                                console.log (s);
                                if (typeof s === "undefined") {
                                    return;
                                }
                                self.successCallback(self.res, {
                                    count: 0,
                                    url: ''
                                });
                            } else {
                                self.getData(true);
                            }
                        }
                    }
                });
            });
        };

        var field = 'number_calculate, day, month, year, start_year, number_addition, number_square, count_number, parent_calculate, json_data_tir, num1';

        for (var i = 2; i <= 80; i++) {
            field += ", num" + i;
        }

        field += ', number_stack, stage, offset_limit';

        this.saveStat = function () {
            var len = [
                self.dataFirst.length,
                self.dataSecond.length
            ];

            for (var i = 0; i < len[0]; i++) {
                var first = self.dataFirst[i];
                for (var j = 0; j < len[1]; j++) {
                    var second = self.dataSecond[j];
                    var count = 0;
                    var saved = [];
                    var json = JSON.parse(first['json_data_tir']);

                    var secondJson = JSON.parse(second['json_data_tir']);
                    for (var l = 0; l < secondJson.length; l++) {
                        json.push(secondJson[l]);
                    }
                    for (var k = 1; k <= 80; k++) {
                        if (first['num' + k] && second['num' + k]) {
                            count++;
                            saved.push(1);
                        } else {
                            saved.push(0);
                        }
                    }

                    if (count>= self.minimalCount) {
                        self.countCount++;
                        var values = self.numberCalculate + ', 0, 0, 0,' + self.startYear + ', 0, 0, ' + count + ', ' +
                                     first['parent_calculate'] + ', \'' + JSON.stringify(json) + '\', ' + saved.join(', ') +
                                     ', ' + parseInt(parseInt(self.numberStack) + 1) + ', ' + self.numberStage + ', \'' + self.limitOffset + '\'';
                        self.dbAdd.addDataTable.addData(values);
                    }
                    //delete saved;
                }
            }
            self.dbAdd.addDataTable.insert(true);
        };

        this.checkExists = function (success) {
            var queryString = 'SELECT count(id) as cnt FROM ' + this.connector.tableName + ' WHERE '
                              + ' number_calculate = ' + this.numberCalculate + ' AND'
                              + ' stage = ' + this.numberStage + ' AND'
                              + ' number_stack = ' + parseInt(parseInt(this.numberStack)) + ' AND'
                              + ' start_year = ' + this.startYear + ' AND'
                              + ' offset_limit = \'' + this.limitOffset + '\'';

            self.connector.connectionMysql.query(queryString, function (err, row) {
                if (err) {
                } else {
                    if (row[0].cnt == 0) {
                        success();
                    } else {
                        self.successCallback(self.res, {
                            count: 0,
                            url:''
                        })
                    }
                }
            });
        };
        var self = this;
        this.dbAdd = database(field);

        return this;
    }
};